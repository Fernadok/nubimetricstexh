#NBM
Requisitos:
    -NETCORE 5
    -SQL SERVER
    
Instrución:
    - 1 Ingresar a la carpeta "SCRIPT DATABASE" y ejecutar en la IDE de SQL SERVER el archivo "CreacionBaseDeDatos(v.2014).sql"
    - 2 Abrir Solución en carpeta "NBM.WebAPI" => "NBM.sln" con Visual Studio 2019 Versión 16.3+.
    - 3 Modificar el ConnectionStrings => "appsettings.Development.json"
    - 4 Presionar F5.
