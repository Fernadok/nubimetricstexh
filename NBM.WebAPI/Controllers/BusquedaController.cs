﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NBM.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBM.WebAPI.Controllers
{
    [ApiController]
    [Route("BusquedaRestfulApp")]
    public class BusquedaController : ControllerBase
    {
        private readonly IBusquedaService busquedaService;
        private readonly IConfiguration configurartion;
        private readonly string urlTermino;

        public BusquedaController(IConfiguration configurartion, IBusquedaService busquedaService)
        {
            this.configurartion = configurartion;
            this.urlTermino = this.configurartion["UrlExternas:Termino"];
            this.busquedaService = busquedaService;
        }

        [HttpGet]
        [Route("busqueda/{termino}")]
        public async Task<ActionResult> Get(string termino)
        {
            return Ok(await this.busquedaService.ObtenerTermino(string.Format(this.urlTermino, termino.ToUpper())));
        }
    }
}
