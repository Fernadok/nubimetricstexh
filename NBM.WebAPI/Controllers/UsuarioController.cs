﻿using Microsoft.AspNetCore.Mvc;
using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.WebAPI.Controllers
{
    [ApiController]
    [Route("UsuariosRestfulApp")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService)
        {
            this._usuarioService = usuarioService;
        }

        [HttpGet]
        [Route("usuarios")]
        public async Task<IEnumerable<UsuarioDto>> Get()
        {
            return await _usuarioService.ObtenerUsuariosAsync();
        }

        [HttpGet]
        [Route("usuarios/{id}")]
        public async Task<UsuarioDto> Get(long id)
        {
            return await _usuarioService.ObtenerUsuarioPorIdAsync(id);
        }

        [HttpPost]
        [Route("usuarios")]
        public async Task<int> Post(UsuarioInputDto usuario)
        {
            return await _usuarioService.InsertarUsuarioAsync(usuario);
        }

        [HttpPut]
        [Route("usuarios")]
        public async Task<bool> Put(UsuarioInputDto usuario)
        {
            return await _usuarioService.EditarUsuarioPorIdAsync(usuario);
        }

        [HttpDelete]
        [Route("usuarios/{id}")]
        public async Task<bool> Delete(long id)
        {
            return await _usuarioService.EliminarUsuarioAsync(id);
        }
    }
}
