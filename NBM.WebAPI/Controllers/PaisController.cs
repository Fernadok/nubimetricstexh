﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NBM.Application.Interfaces;
using NBM.WebAPI.Validaciones;
using System.Threading.Tasks;

namespace NBM.WebAPI.Controllers
{
    [ApiController]
    [Route("PaisesRestfulApp")]
    public class PaisController : ControllerBase
    {
        private readonly IConfiguration configurartion;
        private readonly IPaisService paisService;
        private readonly string urlPaises;

        public PaisController(IConfiguration configurartion, IPaisService paisService)
        {
            this.configurartion = configurartion;
            this.urlPaises = this.configurartion["UrlExternas:Paises"];
            this.paisService = paisService;
        }

        [HttpGet]
        [Route("paises/{pais}")]
        public async Task<ActionResult> Get(string pais)
        {
            var isValid = pais.ValidarPais();
            if (!isValid.Item1) return isValid.Item2;

            var response = await paisService.ObtenerPais(string.Format(this.urlPaises, pais.ToUpper()));
            return Ok(response);
        }
    }
}
