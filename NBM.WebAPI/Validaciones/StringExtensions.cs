﻿using Microsoft.AspNetCore.Mvc;
using NBM.Infrastructure.Enums;
using System;

namespace NBM.WebAPI.Validaciones
{
    public static class StringExtensions
    {
        public static (bool, StatusCodeResult) ValidarPais(this string value)
        {
            return value.ToUpper().Equals(PaisEnum.AR.ToString()) ? (true, new OkResult())
                   : value.ToUpper().Equals(PaisEnum.BR.ToString()) || value.ToUpper().Equals(PaisEnum.CO.ToString()) ? (false, new UnauthorizedResult())
                   : (false, new NoContentResult());
        }
    }
}
