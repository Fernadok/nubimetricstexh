﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NBM.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBM.WebAPI.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class DivisaMiddleware
    {
        private readonly RequestDelegate _next;
        private IMonedaService _monedaService;
        private readonly IConfiguration configurartion;
        private readonly string urlDivisas;
        private readonly string urlConversionDolar;
        private bool firstLoad;

        public DivisaMiddleware(RequestDelegate next
            , IConfiguration configurartion
            , bool firstLoad)
        {
            this._next = next;
            this.firstLoad = firstLoad;
            this.configurartion = configurartion;
            this.urlDivisas = this.configurartion["UrlExternas:Currencies"];
            this.urlConversionDolar = this.configurartion["UrlExternas:CurrencyConversions"];
        }

        public Task Invoke(HttpContext httpContext, IMonedaService monedaService)
        {
            if (this.firstLoad)
            {
                _monedaService = monedaService;
                _monedaService.ObtenerDivisas(urlDivisas, urlConversionDolar);
                this.firstLoad = false;
            }

            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class DivisaMiddlewareExtensions
    {
        public static IApplicationBuilder UseDivisaMiddleware(this IApplicationBuilder builder, bool firstLoad)
        {
            return builder.UseMiddleware<DivisaMiddleware>(firstLoad);
        }
    }
}
