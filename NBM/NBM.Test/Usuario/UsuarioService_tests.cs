﻿using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Test.Usuario
{
    [TestFixture]
    public class UsuarioService_tests
    {
        private readonly IUsuarioService _usuarioService;
        public UsuarioService_tests(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [Test]
        public async Task ObtenerUsuariosAsync()
        {
            // Act
            List<UsuarioDto> output = (List<UsuarioDto>)await _usuarioService.ObtenerUsuariosAsync();

            // Assert
            output.Count.ShouldBeGreaterThan(0);
        }

        [Test]
        public async Task InsertarUsuarioAsync()
        {
            // Act
            int id = await _usuarioService.InsertarUsuarioAsync(new UsuarioInputDto
            {
                Id = 0,
                Apellido = "Apellido A",
                Nombre = "Nombre A",
                Email = "test@testmail.com",
                Password = "12345678"
            });

            UsuarioDto output = await _usuarioService.ObtenerUsuarioPorIdAsync(id);

            // Assert
            output.ShouldNotBeNull();
        }

        [Test]
        public async Task EditarUsuarioPorIdAsync()
        {
            // Act
            var usuario = await _usuarioService.ObtenerUsuarioPorIdAsync(1);

            UsuarioInputDto input = new UsuarioInputDto
            {
                Apellido = "UPD Apellido",
                Nombre = "UPD Nombre",
                Email= "UPDATE@gmaii.com",
                Password = "UPD Pass987654"
            };
            bool output = await _usuarioService.EditarUsuarioPorIdAsync(input);

            // Assert
            output.ShouldBeTrue();
        }

    }
}
