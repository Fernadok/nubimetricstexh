﻿using Microsoft.Extensions.DependencyInjection;
using NBM.Application.Interfaces;
using NBM.Application.Mapping;
using NBM.Application.Services;
using NBM.Infrastructure.Interfaces.Repositories;
using NBM.Infrastructure.Repositories;
using System.Net.Http;
using System.Reflection;
using System.Threading;

namespace NBM.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddCustomConfiguredAutoMapper();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IUsuarioService, UsuarioService>();
            services.AddTransient<IBusquedaService, BusquedaService>();
            services.AddTransient<IPaisService, PaisService>();
            services.AddTransient<IMonedaService, MonedaService>();
            services.AddScoped<IAlmacenarArchivos, AlmacenarArchivos>();
            services.AddSingleton<HttpClient>();
  
            return services;
        }
    }
}
