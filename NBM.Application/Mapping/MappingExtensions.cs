﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NBM.Application.Mapping
{
    public static class MappingExtensions
    {
        public static  List<TDestination> ProjectToListAsync<TDestination>(this IQueryable queryable
            , IConfigurationProvider configuration)
        {
            return  queryable.ProjectTo<TDestination>(configuration).ToList();
        }
    }

}
