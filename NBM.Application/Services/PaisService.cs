﻿using NBM.Application.Interfaces;
using NBM.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Application.Services
{
    public class PaisService : IPaisService
    {
        public async Task<string> ObtenerPais(string urlPais)
        {
            return await ClienteRest.Get(urlPais);
        }
    }
}
