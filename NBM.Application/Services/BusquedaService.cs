﻿using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using NBM.Infrastructure;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBM.Application.Services
{
    public class BusquedaService : IBusquedaService
    {
        public async Task<IList<TerminoDto>> ObtenerTermino(string urlTermino)
        {
            var response = await ClienteRest.Get(urlTermino);
            JObject json = JObject.Parse(response);
            JArray results = (JArray)json["results"];

            IList<TerminoDto> teminos = results.Select(iten => new TerminoDto
            {
                Id = (string)iten["id"],
                SiteId = (string)iten["site_id"],
                Title = (string)iten["title"],
                Price = (long)iten["price"],
                SellerId = (string)iten["seller"]["id"],
                Permalink = (string)iten["permalink"]
            }).ToList();

            return teminos;
        }
    }
}
