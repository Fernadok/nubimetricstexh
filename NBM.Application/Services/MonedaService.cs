﻿using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using NBM.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace NBM.Application.Services
{
    public class MonedaService : IMonedaService
    {
        private readonly IAlmacenarArchivos almacenarArchivos;
        private readonly HttpClient httpClient;

        public MonedaService(IAlmacenarArchivos almacenarArchivos, HttpClient httpClient)
        {
            this.almacenarArchivos = almacenarArchivos;
            this.httpClient = httpClient;
        }

        public async Task ObtenerDivisas(string urlDivisas, string urlConversionDolar)
        {
            var divisas = JsonConvert.DeserializeObject<List<MonedaDto>>(await ClienteRest.Get(urlDivisas));
            await Conversiones(urlConversionDolar, divisas);
        }

        private async Task Conversiones(string urlConversionDolar, List<MonedaDto> divisas)
        {
            var tareas = new List<Task<HttpResponseMessage>>();

            tareas = divisas.Select(async divisa =>
            {
               return await httpClient.GetAsync(string.Format(urlConversionDolar, divisa.Id));
            }).ToList();

            var respuestasTareas = await Task.WhenAll(tareas);
            foreach (var respuesta in respuestasTareas)
            {
                var contenido = await respuesta.Content.ReadAsStringAsync();
                var respuestaTarjeta = JsonConvert.DeserializeObject<ToDolar>(contenido);
                if (respuestaTarjeta.Currency_base != null)
                    divisas.FirstOrDefault(s => s.Id == respuestaTarjeta.Currency_base).ToDolar = respuestaTarjeta;
            }

            await almacenarArchivos.GuardarArchivoJSON("json", divisas);
            await almacenarArchivos.GuardarArchivoCSV("csv", divisas);
        }
    }
}
