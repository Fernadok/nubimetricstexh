﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.Services
{
    public class AlmacenarArchivos : IAlmacenarArchivos
    {
        private readonly IHostingEnvironment env;

        public AlmacenarArchivos(IHostingEnvironment env)
        {
            this.env = env;
        }

        public async Task<string> GuardarArchivoJSON(string contenedor, List<MonedaDto> divisas)
        {
            string nombreArchivo = $"divisas_{DateTime.Now.Ticks}.json";
            string carpeta = Path.Combine(env.WebRootPath, contenedor);
            if (!Directory.Exists(carpeta)) Directory.CreateDirectory(carpeta);

            string ruta = Path.Combine(carpeta, nombreArchivo);
            await File.WriteAllTextAsync(ruta, JsonConvert.SerializeObject(divisas));

            var rutaLocal = Path.Combine("/", contenedor, nombreArchivo).Replace("\\", "/");
            return rutaLocal;
        }

        public async Task<string> GuardarArchivoCSV(string contenedor, List<MonedaDto> divisas)
        {
            string nombreArchivo = $"divisas_{DateTime.Now.Ticks}.csv";
            string carpeta = Path.Combine(env.WebRootPath, contenedor);
            if (!Directory.Exists(carpeta)) Directory.CreateDirectory(carpeta);

            string ruta = Path.Combine(carpeta, nombreArchivo);

            var csv = new StringBuilder();
            divisas.ForEach(line =>
            {
                csv.AppendLine(line.ToDolar?.Ratio.ToString());
            });

            await File.WriteAllTextAsync(ruta, csv.ToString());

            var rutaLocal = Path.Combine("/", contenedor, nombreArchivo).Replace("\\", "/");
            return rutaLocal;
        }

    }
}
