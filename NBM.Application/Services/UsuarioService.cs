﻿using AutoMapper;
using NBM.Application.DTOs;
using NBM.Application.Interfaces;
using NBM.Domain.Entities;
using NBM.Infrastructure.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Application.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMapper _mapper;

        public UsuarioService(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            this._usuarioRepository = usuarioRepository;
            _mapper = mapper;
        }

        public async Task<bool> EditarUsuarioPorIdAsync(UsuarioInputDto usuario)
        {
            var usuarioMap = _mapper.Map<Usuario>(usuario);
            return await this._usuarioRepository.EditarUsuarioPorIdAsync(usuarioMap);
        }

        public async Task<bool> EliminarUsuarioAsync(long id)
        {
            return await this._usuarioRepository.EliminarUsuarioAsync(id);
        }

        public async Task<int> InsertarUsuarioAsync(UsuarioInputDto usuario)
        {
            var usuarioMap = _mapper.Map<Usuario>(usuario);
            return await this._usuarioRepository.InsertarUsuarioAsync(usuarioMap);
        }

        public async Task<UsuarioDto> ObtenerUsuarioPorIdAsync(long id)
        {
            var resultado = await this._usuarioRepository.ObtenerUsuarioPorIdAsync(id);
            return _mapper.Map<UsuarioDto>(resultado);
        }

        public async Task<IEnumerable<UsuarioDto>> ObtenerUsuariosAsync()
        {
            var resultados = await this._usuarioRepository.ObtenerUsuariosAsync();
            return _mapper.Map<List<UsuarioDto>>(resultados);
        }
    }
}
