﻿using AutoMapper;
using NBM.Application.Mapping;
using NBM.Domain.Entities;

namespace NBM.Application.DTOs
{
    public class UsuarioDto : IMapFrom<Usuario>
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Usuario, UsuarioDto>();
        }
    }
}
