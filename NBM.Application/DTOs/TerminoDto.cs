﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.DTOs
{
    public class TerminoDto
    {
        public string Id { get; set; }
        public string SiteId { get; set; }
        public string Title { get; set; }
        public long Price { get; set; }
        public string SellerId { get; set; }
        public string Permalink { get; set; }
    }
}
