﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.DTOs
{
    public class MonedaDto
    {
        public string Id { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public int Decimal_Places { get; set; }
        public ToDolar ToDolar { get; set; }
    }
}
