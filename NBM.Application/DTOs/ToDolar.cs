﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.DTOs
{
    public class ToDolar
    {
        public string Currency_base { get; set; }
        public string Currency_quote { get; set; }
        public float Ratio { get; set; }
        public float Rate { get; set; }
        public float Inv_Rate { get; set; }
        public DateTime Creation_Date { get; set; }
        public DateTime Valid_Until { get; set; }
    }

}
