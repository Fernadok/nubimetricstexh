﻿using System.Threading.Tasks;

namespace NBM.Application.Interfaces
{
    public interface IPaisService
    {
        Task<string> ObtenerPais(string urlPais);
    }
}
