﻿using NBM.Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.Interfaces
{
    public interface IBusquedaService
    {
        Task<IList<TerminoDto>> ObtenerTermino(string urlTermino);
    }
}
