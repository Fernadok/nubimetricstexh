﻿using System.Threading.Tasks;

namespace NBM.Application.Interfaces
{
    public interface IMonedaService
    {
        Task ObtenerDivisas(string urlDivisas, string urlConversionDolar);
    }
}
