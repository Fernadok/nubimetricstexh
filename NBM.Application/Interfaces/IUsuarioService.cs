﻿using NBM.Application.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Application.Interfaces
{
    public interface IUsuarioService
    {
        Task<IEnumerable<UsuarioDto>> ObtenerUsuariosAsync();

        Task<UsuarioDto> ObtenerUsuarioPorIdAsync(long id);

        Task<int> InsertarUsuarioAsync(UsuarioInputDto usuario);

        Task<bool> EditarUsuarioPorIdAsync(UsuarioInputDto usuario);

        Task<bool> EliminarUsuarioAsync(long id);
    }
}
