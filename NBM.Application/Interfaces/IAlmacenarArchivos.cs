﻿using NBM.Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBM.Application.Interfaces
{
    public interface IAlmacenarArchivos
    {
        Task<string> GuardarArchivoJSON(string contenedor, List<MonedaDto> divisas);
        Task<string> GuardarArchivoCSV(string contenedor, List<MonedaDto> divisas);
    }
}
