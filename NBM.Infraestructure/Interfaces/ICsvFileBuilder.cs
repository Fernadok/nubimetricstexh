﻿using System.Collections.Generic;

namespace NBM.Infrastructure.Interfaces
{
    public interface ICsvFileBuilder<T>
    {
        byte[] BuildTodoItemsFile(IEnumerable<T> records);
    }
}
