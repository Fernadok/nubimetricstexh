﻿using NBM.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Infrastructure.Interfaces.Repositories
{
    public interface IUsuarioRepository
    {
        Task<IEnumerable<Usuario>> ObtenerUsuariosAsync();

        Task<Usuario> ObtenerUsuarioPorIdAsync(long id);

        Task<int> InsertarUsuarioAsync(Usuario usuario);

        Task<bool> EditarUsuarioPorIdAsync(Usuario usuario);

        Task<bool> EliminarUsuarioAsync(long id);
    }
}
