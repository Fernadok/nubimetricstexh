﻿using NBM.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace NBM.Infrastructure.Dapper.DAOs
{
    public static class UsuarioDao
    {
        public static async Task<IEnumerable<Usuario>> ObtenerUsuariosAsync(this IDbConnection connection)
        {
            using var conn = connection;
            conn.Open();

            var sql = @"SELECT Id, Nombre, Apellido, Email, Password 
                        FROM USUARIOS";

            return (await conn.QueryAsync<Usuario>(sql));
        }

        public static async Task<Usuario> ObtenerUsuarioPorIdAsync(this IDbConnection connection, long id)
        {
            using var conn = connection;
            conn.Open();

            var param = new DynamicParameters();
            param.Add("@Id", id, DbType.Int64);

            var sql = @"SELECT Id, Nombre, Apellido, Email, Password 
                        FROM USUARIOS WHERE Id = @Id";

            return (await conn.QueryAsync<Usuario>(sql, param)).SingleOrDefault();
        }


        public static async Task<int> InsertarUsuarioAsync(this IDbConnection connection, Usuario usuario)
        {
            using var conn = connection;
            conn.Open();

            var sql = @"INSERT INTO USUARIOS (Nombre, Apellido, Email, Password) 
                        VALUES (@Nombre, @Apellido, @Email, @Password)
                        SELECT CAST(SCOPE_IDENTITY() as int)";

            return (await connection.ExecuteScalarAsync<int>(sql, usuario));
        }

        public static async Task<bool> EditarUsuarioPorIdAsync(this IDbConnection connection, Usuario usuario)
        {
            using var conn = connection;
            conn.Open();

            var sql = @"UPDATE USUARIOS
                        SET Nombre = @Nombre, 
                            Apellido = @Apellido, 
                            Email = @Email, 
                            Password = @Password
                        WHERE Id = @Id";

            await connection.ExecuteAsync(sql, usuario);

            return true;
        }

        public static async Task<bool> EliminarUsuarioAsync(this IDbConnection connection, long id)
        {
            using var conn = connection;
            conn.Open();

            var param = new DynamicParameters();
            param.Add("@Id", id, DbType.Int64);

            var sql = @"DELETE FROM USUARIOS WHERE Id = @Id";
            await connection.ExecuteAsync(sql, new { Id = id });

            return true;
        }
    }
}
