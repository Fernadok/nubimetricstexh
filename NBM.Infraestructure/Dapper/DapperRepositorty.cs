﻿using Microsoft.Extensions.Configuration;
using NBM.Infrastructure.Dapper.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace NBM.Infrastructure.Dapper
{
    public class DapperRepositorty : IDapperRepositorty
    {
        private readonly IConfiguration configuration;
        protected IDbConnection connection => new SqlConnection(configuration.GetConnectionString("Default"));

        public DapperRepositorty(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
    }
}
