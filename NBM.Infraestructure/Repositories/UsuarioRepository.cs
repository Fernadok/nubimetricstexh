﻿using Microsoft.Extensions.Configuration;
using NBM.Domain.Entities;
using NBM.Infrastructure.Dapper;
using NBM.Infrastructure.Dapper.DAOs;
using NBM.Infrastructure.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NBM.Infrastructure.Repositories
{
    public class UsuarioRepository : DapperRepositorty, IUsuarioRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration">IConfiguration</param>
        public UsuarioRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<bool> EditarUsuarioPorIdAsync(Usuario usuario)
        {
            return await this.connection.EditarUsuarioPorIdAsync(usuario);
        }

        public async Task<bool> EliminarUsuarioAsync(long id)
        {
            return await this.connection.EliminarUsuarioAsync(id);
        }

        public async Task<int> InsertarUsuarioAsync(Usuario usuario)
        {
            return await this.connection.InsertarUsuarioAsync(usuario);
        }

        public async Task<Usuario> ObtenerUsuarioPorIdAsync(long id)
        {
            return await this.connection.ObtenerUsuarioPorIdAsync(id);
        }

        public async Task<IEnumerable<Usuario>> ObtenerUsuariosAsync()
        {
            return await this.connection.ObtenerUsuariosAsync();
        }
    }
}
