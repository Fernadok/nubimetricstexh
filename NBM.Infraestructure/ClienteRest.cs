﻿using RestSharp;
using System.Threading.Tasks;

namespace NBM.Infrastructure
{
    public class ClienteRest
    {
        public static async Task<string> Get(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            var response = await client.ExecuteAsync(request);

            return response.Content;
        }
    }
}
